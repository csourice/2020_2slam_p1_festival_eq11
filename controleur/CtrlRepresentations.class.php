<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\DaoRepresentation;
use modele\metier\Representation;
use modele\dao\DaoLieu;
use modele\dao\GroupeDAO;
use modele\dao\Bdd;
use vue\representations\VueListeRepresentations;


class CtrlRepresentations extends ControleurGenerique {

    /** controleur= representations & action= defaut
     * Afficher la liste des representations     */
    public function defaut() {
        $this->liste();
    }

    /** controleur= representations & action= liste
     * Afficher la liste des établissements      */
    public function liste() {
        $laVue = new VueListeRepresentations();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des representations 
        Bdd::connecter();
        $laVue->setLesRepresentations($this->getTabRepresentations());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representations");
        $this->vue->afficher();
    }

    /** controleur= representations & action=creer
     * Afficher le formulaire d'ajout d'une representation     */
    public function creer() {
        $laVue = new VueSaisieEtablissement();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouvel établissement");
        // En création, on affiche un formulaire vide
        /* @var Etablissement $unEtab */
        $unEtab = new Etablissement("", "", "", "", "", "", "", 0, "Monsieur", "", "");
        $laVue->setUnEtablissement($unEtab);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - etablissements");
        $this->vue->afficher();
    }

    /** controleur= representations & action=validerCreer
     * ajout d'une representation dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        /* @var Etablissement $unEtab  : récupération du contenu du formulaire et instanciation d'un établissement */
        $unEtab = new Etablissement($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['adresseRue'], $_REQUEST['codePostal'], $_REQUEST['ville'], $_REQUEST['tel'], $_REQUEST['adresseElectronique'], $_REQUEST['type'], $_REQUEST['civiliteResponsable'], $_REQUEST['nomResponsable'], $_REQUEST['prenomResponsable']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesEtab($unEtab, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer l'établissement
            EtablissementDAO::insert($unEtab);
            // revenir à la liste des établissements
            header("Location: index.php?controleur=etablissements&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieEtablissement();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouvel établissement");
            $laVue->setUnEtablissement($unEtab);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - etablissements");
            $this->vue->afficher();
        }
    }

    /** controleur= representations & action=modifier $ id=identifiant de l'établissement à modifier
     * Afficher le formulaire de modification d'une representation     */
    public function modifier() {
        $idEtab = $_GET["id"];
        $laVue = new VueSaisieEtablissement();
        $this->vue = $laVue;
        // Lire dans la BDD les données de l'établissement à modifier
        Bdd::connecter();
        /* @var Etablissement $leEtablissement */
        $leEtablissement = EtablissementDAO::getOneById($idEtab);
        $this->vue->setUnEtablissement($leEtablissement);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier l'établissement : " . $leEtablissement->getNom() . " (" . $leEtablissement->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - etablissements");
        $this->vue->afficher();
    }

    /** controleur= representations & action=validerModifier
     * modifier une representation dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Etablissement $unEtab  : récupération du contenu du formulaire et instanciation d'un établissement */
        $unEtab = new Etablissement($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['adresseRue'], $_REQUEST['codePostal'], $_REQUEST['ville'], $_REQUEST['tel'], $_REQUEST['adresseElectronique'], $_REQUEST['type'], $_REQUEST['civiliteResponsable'], $_REQUEST['nomResponsable'], $_REQUEST['prenomResponsable']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesEtab($unEtab, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour l'établissement
            EtablissementDAO::update($unEtab->getId(), $unEtab);
            // revenir à la liste des établissements
            header("Location: index.php?controleur=etablissements&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieEtablissement();
            $this->vue = $laVue;
            $laVue->setUnEtablissement($unEtab);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier l'établissement : " . $unEtab->getNom() . " (" . $unEtab->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - etablissements");
            $this->vue->afficher();
        }
    }

    /** controleur= representations & action=supprimer & id=identifiant_representation
     * Supprimer une representation d'après son identifiant     */
    public function supprimer() {
        $idRep = $_GET["id"];
        $this->vue = new VueSupprimerRepresentation();
        // Lire dans la BDD les données de la representation à supprimer
        Bdd::connecter();
        $this->vue->setUneRepresentation(DaoRepresentation::getOneById($idRep));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }//Polo was here
    

    /** controleur= representations & action= validerSupprimer
     * supprimer une representation dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de l'établissement à supprimer");
        } else {
            // suppression de l'établissement d'après son identifiant
            EtablissementDAO::delete($_GET["id"]);
        }
        // retour à la liste des établissements
        header("Location: index.php?controleur=etablissements&action=liste");
    }

    /**
     * Vérification des données du formulaire de saisie
     * @param Etablissement $unEtab établissement à vérifier
     * @param bool $creation : =true si formulaire de création d'un nouvel établissement ; =false sinon
     */
    private function verifierDonneesEtab(Etablissement $unEtab, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $unEtab->getId() == "") || $unEtab->getNom() == "" || $unEtab->getAdresse() == "" || $unEtab->getCdp() == "" ||
                $unEtab->getVille() == "" || $unEtab->getTel() == "" || $unEtab->getNomResp() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unEtab->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unEtab->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (EtablissementDAO::isAnExistingId($unEtab->getId())) {
                    GestionErreurs::ajouter("L'établissement " . $unEtab->getId() . " existe déjà");
                }
            }
        }
        // Vérification qu'un établissement de même nom n'existe pas déjà (id + nom si création)
        if ($unEtab->getNom() != "" && EtablissementDAO::isAnExistingName($creation, $unEtab->getId(), $unEtab->getNom())) {
            GestionErreurs::ajouter("L'établissement " . $unEtab->getNom() . " existe déjà");
        }
        // Vérification du format du code postal
        if ($unEtab->getCdp() != "" && !estUnCp($unEtab->getCdp())) {
            GestionErreurs::ajouter('Le code postal doit comporter 5 chiffres');
        }
    }

    /*****************************************************************************
     * Méthodes permettant de préparer les informations à destination des vues
     ******************************************************************************/

    /**
     * Retourne la liste de tous les Etablissements et du nombre d'attributions de chacun
     * @return Array tableau associatif à 2 dimensions : 
     *      - dimension 1, l'index est l'id de l'établissement
     *      - dimension 2, index "etab" => objet de type Etablissement
     *      - dimension 2, index "nbAttrib" => nombre d'attributions pour cet établissement
     */
    public function getTabRepresentations(): Array {
        $lesRepresentations = Array();
        $lesRepresentations = DaoRepresentation::getAll();
        
        return $lesRepresentations;
    }

}
