<?php
namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Groupe;
use modele\dao\DaoLieu;
use modele\dao\GroupeDAO;
use PDO;

/**
 * Description of DaoRepresentation
 * Classe métier :  Representation
 * @author eleve
 * @version 2020
 */
class DaoRepresentation {
    
    /**
     * Instancier un objet de la classe Representation à partir des tables Representation, Lieux, Groupe
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID_REPRESENTATION'];
        $idGroupe = $enreg['ID_GROUPE'];
        $idLieu = $enreg['ID_LIEU'];
        $dateRep = $enreg['DATE'];
        $heureDebut = $enreg['HEURE_DEBUT'];
        $heureFin = $enreg['HEURE_FIN'];
       
        $objetLieu = DaoLieu::getOneById($idLieu);
        $objetGroupe = GroupeDAO::getOneById($idGroupe);
  
        $uneRepresentation = new Representation($id, $objetLieu , $objetGroupe, $dateRep, $heureDebut, $heureFin);
        return $uneRepresentation;
    }
    protected static function enregVersMetierDates(array $enreg) {
        $dates = $enreg['DATE'];       
        return ($dates);
    }
    
    /**
     * Retourne la liste de toutes les representations 
     * @return array tableau d'objets de representations
     */    
    public static function getAll() {
          $lesObjets = array();
          $requete = "SELECT * FROM Representation";
          $stmt = Bdd::getPdo()->prepare($requete);
          $ok = $stmt->execute();
          if ($ok) {

              while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {

                  $lesObjets[] = self::enregVersMetier($enreg);
              }
          }
          return $lesObjets;
      }
      public static function getAllDate() {
          $lesDates = array();
          $requete = "SELECT DATE FROM Representation";
          $stmt = Bdd::getPdo()->prepare($requete);
          $ok = $stmt->execute();
          if ($ok) {

              while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {

                  $lesDates[] = self::enregVersMetierDates($enreg);
              }
          }
          return $lesDates;
      }
          public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }
    
}