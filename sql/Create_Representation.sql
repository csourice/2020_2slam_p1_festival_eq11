/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  Mathias
 * Created: 6 oct. 2020
 */

CREATE TABLE `festival`.`Representation` ( 
    `id_representation` INT(3) NOT NULL AUTO_INCREMENT ,  
    `id_groupe` char(4) NOT NULL , 
    `id_lieu` INT(3) NOT NULL , 
    `date` DATE NOT NULL , 
    `heure_debut` char(20) NOT NULL , 
    `heure_fin` char(20) NOT NULL , 
    PRIMARY KEY (`id_representation`)) ENGINE = InnoDB; 